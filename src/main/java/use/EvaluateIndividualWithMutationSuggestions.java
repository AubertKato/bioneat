package use;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.chemicals.SequenceVertex;
import reactionnetwork.Connection;
import reactionnetwork.ConnectionSerializer;
import reactionnetwork.ReactionNetwork;
import reactionnetwork.ReactionNetworkDeserializer;
import use.processing.multiobjective.RDInObjective;
import use.processing.multiobjective.RDOutObjective;
import use.processing.rd.PatternEvaluator;
import use.processing.rd.RDConstants;
import use.processing.rd.RDImagePrinter;
import use.processing.rd.RDPatternFitnessResult;
import use.processing.rd.RDPatternFitnessResultIbuki;
import use.processing.rd.RDSystem;
import use.processing.rd.RDSystemApprox;
import utils.RunningStatsAnalysis;

public class EvaluateIndividualWithMutationSuggestions extends EvaluateIndividualWithDescriptors {
	
	public static int baseExtraSteps = 10;

	public static void main(String[] args) {
		Gson gson = new GsonBuilder().registerTypeAdapter(ReactionNetwork.class, new ReactionNetworkDeserializer())
				.registerTypeAdapter(Connection.class, new ConnectionSerializer()).create();

		
		if(args.length >= 2){
			RDSystem[] systems = launch(args[0],args[1]);
			
			RDPatternFitnessResult[] fitness = new RDPatternFitnessResult[targets.length];
			RDInObjective inObjective = new RDInObjective();
			RDOutObjective outObjective = new RDOutObjective();
			
			Scanner scan = new Scanner(System.in);
			
			while(true) {
				System.out.println("DONE");
				String command = scan.nextLine();
				if(command.toLowerCase().equals("exit")) {
					break;
				} else {
					String[] params = command.split("\\s");
					BufferedReader br;
					try {
						int extraSteps = params.length>1?Integer.parseInt(params[1]):baseExtraSteps;
						RunningStatsAnalysis[] rsa = new RunningStatsAnalysis[targets.length];
						for(int j = 0; j< targets.length; j++ ) {
							//targets[j] = setTarget(targetNames[j].trim());
							rsa[j] = new RunningStatsAnalysis();
						}
						StringBuilder sb = new StringBuilder("");
						
						//select a new reaction network
						br = new BufferedReader(new FileReader(params[0]));
						ReactionNetwork nreac = gson.fromJson(br, ReactionNetwork.class);
						//now make a clone of the RDSystems and run them for some additional time before evaluating again
						for(int i = 0; i<systems.length; i++) {
							RDSystem newSystem = useApprox? new RDSystemApprox(): new RDSystem();
							setTestGraph(newSystem,nreac);
							newSystem.init(false); //with full power, because we are doing parallel eval (maybe)
							
							if(RDConstants.debug) RDImagePrinter.printSystem(systems[i], args[1]+"_before_"+i+".png");
							
							setupNewSystem(systems[i],newSystem);
							
							if(RDConstants.debug) RDImagePrinter.printSystem(newSystem, params[0]+"_before_"+i+".png");
							
							additionalEval(newSystem,extraSteps);
							
							if(RDConstants.debug) RDImagePrinter.printSystem(newSystem, params[0]+"_after_"+i+".png");
							
							 boolean[][] glue = PatternEvaluator.detectGlue(newSystem.conc[RDConstants.glueIndex]);
							  
							  for(int k = 0; k< targets.length; k++ ) {
								  fitness[k] = new RDPatternFitnessResultIbuki(newSystem.conc,targets[k],newSystem.beadsOnSpot,0.0);
								  rsa[k].addData(fitness[k].getFitness());
							  
								  sb.append("fitness"+i+"_"+k+": "+fitness[k]+"\n");
								  sb.append("meansofar"+i+"_"+k+": "+rsa[k].getMean()+"\n");
								  sb.append("sdsofar"+i+"_"+k+": "+rsa[k].getStandardDeviation()+"\n");
								  sb.append("sesofar"+i+"_"+k+": "+rsa[k].getStandardError()+"\n");
								  sb.append("in"+i+"_"+k+": "+inObjective.evaluateScore(nreac, targets[k], glue)+"\n");
								  sb.append("out"+i+"_"+k+": "+(1.0-outObjective.evaluateScore(nreac, targets[k], glue))+"\n");
								  sb.append("hellinger"+i+"_"+k+": "+PatternEvaluator.hellingerDistance(newSystem.conc[RDConstants.glueIndex], targets[k])+"\n");
							  }
							  
							  
							 
						}
						
						System.out.println(sb.toString());
						
					} catch (FileNotFoundException e) {
						
						System.out.println("USAGE: jsonfile [additional steps]");
					}
					
				}
			}
			
		} else {
			System.out.println("USAGE: java [options] "+EvaluateIndividualWithMutationSuggestions.class.getName()+" parameters");
			
		}
		
		System.exit(0);
	}
	
	public static void additionalEval(RDSystem newSys, int length) {
		for(int j = 0; j<length; j++) newSys.update();
	}
	
	/**
	 * Use the last concentration frame of oldSys to setup concentrations. Beads are set to be free and random to shock the system.
	 * New species are set to 0 concentration.
	 * @param oldSys
	 * @param newSys
	 */
	public static void setupNewSystem(RDSystem oldSys, RDSystem newSys) {
		
		for(SequenceVertex s : oldSys.getOS().getSequences()) {
			int oldIndex = oldSys.seqAddress.get(s);
			Integer index = newSys.seqAddress.get(s);
			if(index != null) {
				for(int x = 0; x< oldSys.conc[oldIndex].length; x++) {
					for(int y = 0; y<oldSys.conc[oldIndex][x].length; y++) {
						newSys.conc[index][x][y] = oldSys.conc[oldIndex][x][y];
					}
				}
			}
		}
	}
	
}
